using UnityEngine;

public class ClientData
{
    public float Scale;
    public Vector3 Pos;
    public string ImageName;

    public readonly View View;

    public ClientData()
    {
        Scale = 1;
        Pos = new Vector3();
        ImageName = string.Empty;
        View = View.Create();
        View.data = this;
    }

    public void Destroy()
    {
        Object.Destroy(View.gameObject);
    }
}