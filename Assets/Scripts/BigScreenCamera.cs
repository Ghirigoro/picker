using UnityEngine;

public class BigScreenCamera : MonoBehaviour
{
    public const int ScreenWidth = 1920 * 4;
    public const int ScreenHeight = 1080;

    private Camera _cam;

    private void Awake()
    {
        InitializeCamera();
    }

    private void InitializeCamera()
    {

        _cam = GetComponent<Camera>();
        if (_cam == null)
        {
            _cam = Camera.main;
        }

        if (_cam == null)
        {
            throw new MissingComponentException("Could not find a camera");
        }

        _cam.rect = new Rect(0, 0, ScreenWidth, ScreenHeight);
        _cam.orthographic = true;
        _cam.orthographicSize = ScreenHeight * 0.5f;
        _cam.transform.position = new Vector3(0, 0, -500);
        _cam.nearClipPlane = -500;
        _cam.farClipPlane = 500;

        foreach (var display in Display.displays)
        {
            display.Activate(1980, 1080, 60);
        }

        Screen.SetResolution(ScreenWidth, ScreenHeight, FullScreenMode.Windowed);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            // Screen.fullScreen = !Screen.fullScreen;

            switch (Screen.fullScreenMode)
            {
                case FullScreenMode.ExclusiveFullScreen:
                    Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
                    break;
                case FullScreenMode.FullScreenWindow:
                    Screen.fullScreenMode = FullScreenMode.MaximizedWindow;
                    break;
                case FullScreenMode.MaximizedWindow:
                    Screen.fullScreenMode = FullScreenMode.Windowed;
                    break;
                case FullScreenMode.Windowed:
                    Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
                    break;
            }
        }
    }

    private void Reset()
    {
        InitializeCamera();
    }

}