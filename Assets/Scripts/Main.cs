using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

public class Main : MonoBehaviour
{
    private Connection _connection;

#if UNITY_EDITOR
    private const string SocketUrl = "http://localhost:5000/socket.io/";
    private const string ImagesUrl = "http://localhost:5000/picker/images/";
# else
    private const string SocketUrl = "http://bigscreens.herokuapp.com/socket.io/";
    private const string ImagesUrl = "http://bigscreens.herokuapp.com/picker/images/";
# endif

    private readonly Dictionary<string, ClientData> _clients = new Dictionary<string, ClientData>();

    private void Awake()
    {
        // Initialize connection
        InitConnection();
    }

    private void InitConnection()
    {
        // Setup the connection
        Debug.Log("Connecting to " + SocketUrl);
        _connection = new Connection(SocketUrl, "Picker", "app");

        _connection.OnConnect(() =>
        {
            Debug.Log("Connected to server.");
            // Start downloading the images off the server
            StartCoroutine(DownloadImages());
        });

        _connection.OnDisconnect(() =>
        {
            Debug.Log("Disconnected to server.");
            ClearAllClients();
        });

        _connection.OnOtherConnect((id, type) =>
       {
           Debug.Log($"OTHER CONNECTED: {type} ({id})");
           if (type == "user") AddClient(id);
       });

        _connection.OnOtherDisconnect((id, type) =>
       {
           Debug.Log($"OTHER DISCONNECTED: {type} ({id})");
           if (type == "user") ClearClient(id);
       });

        _connection.OnError(err =>
        {
            Debug.Log($"Connection error: {err}");
        });

        _connection.On("image_selected", (string sourceId, string imageName) =>
        {
            if (GetDataForClient(sourceId, out var data))
            {
                data.ImageName = imageName.Trim();
            }

        });

        _connection.On("scale", (string sourceId, float scale) =>
        {
            if (GetDataForClient(sourceId, out var data))
            {
                data.Scale = scale;
            }
        });

        _connection.On("move", (string sourceId, float x, float y) =>
        {
            if (GetDataForClient(sourceId, out var data))
            {
                // Browser has inverted y coordinates relative to Unity
                data.Pos = new Vector3(x, 1 - y);
            }
        });

        _connection.Open();
    }

    private static IEnumerator DownloadImages()
    {
        // Get the list of images on the server
        var manifestRequest = UnityWebRequest.Get(ImagesUrl + "manifest.txt");
        yield return manifestRequest.SendWebRequest();
        if (manifestRequest.isNetworkError || manifestRequest.isHttpError)
        {
            Debug.LogWarning($"Could not retrieve image manifest: {manifestRequest.error}");
            yield break;
        }

        var manifest = manifestRequest.downloadHandler.text;
        var imageNames = Regex.Split(manifest, "\r\n|\r|\n");

        if (imageNames.Length == 0)
        {
            Debug.LogWarning("Server image manifest is empty.");
            yield break;
        }

        // Start downloading the images
        var imageDownloads = new List<Tuple<string, UnityWebRequest>>();
        foreach (var imageName in imageNames)
        {
            var request = UnityWebRequestTexture.GetTexture(ImagesUrl + imageName);
            imageDownloads.Add(new Tuple<string, UnityWebRequest>(imageName, request));
            request.SendWebRequest();
        }

        // Wait for them to download
        yield return new WaitUntil(() =>
        {
            foreach (var (_, request) in imageDownloads)
                if (!request.isDone) return false;

            return true;
        });

        // Process the downloaded images
        foreach (var download in imageDownloads)
        {
            var (imageName, request) = download;
            if (request.isNetworkError || request.isHttpError)
                Debug.Log($"Failed to download {imageName}: {request.error}");
            else
            {
                var texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
                View.AddImage(imageName, texture);
            }
        }
    }

    private void OnDestroy()
    {
        _connection.Close();
    }

    private void AddClient(string id)
    {
        if (GetDataForClient(id, out var data)) data.Destroy();
        _clients[id] = new ClientData();
    }

    private void ClearClient(string id)
    {
        if (GetDataForClient(id, out var data)) data.Destroy();
        _clients.Remove(id);
    }

    private void ClearAllClients()
    {
        foreach (var entry in _clients)
        {
            entry.Value.Destroy();
        }
        _clients.Clear();
    }

    private bool GetDataForClient(string clientId, out ClientData data)
    {
        if (clientId == null || !_clients.ContainsKey(clientId))
        {
            data = null;
            return false;
        }

        data = _clients[clientId];
        return true;
    }

}




