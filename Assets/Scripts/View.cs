using System.Collections.Generic;
using UnityEngine;

public class View : MonoBehaviour
{
    private static readonly Dictionary<string, Sprite> Sprites = new Dictionary<string, Sprite>();

    public static void AddImage(string name, Texture2D texture)
    {
        var sprite = Sprite.Create(
            texture, 
            new Rect(0, 0, texture.width, texture.height),
            new Vector2(0.5f, 0.5f),
            1
        );
        sprite.name = name;
        Sprites[name] = sprite;
    }
    
    
    public static View Create()
    {
        var views = GameObject.Find("Views");
        if (views == null)
        {
            views = new GameObject("Views");
        }

        var go = new GameObject("View");
        var view = go.AddComponent<View>();
        go.AddComponent<SpriteRenderer>();
        view.transform.parent = views.transform;
        return view;
    }
    
    public ClientData data;

    private void Update()
    {
        if (Sprites.TryGetValue(data.ImageName, out var sprite))
        {
            var spriteRenderer = GetComponent<SpriteRenderer>();

            if (spriteRenderer.sprite == null || (spriteRenderer.sprite.name != data.ImageName))
            {
                spriteRenderer.sprite = sprite;   
            }
        }

        var newPos = Camera.main.ViewportToWorldPoint(data.Pos);
        newPos.z = 0;
        transform.localPosition = newPos;
        transform.localScale = new Vector3(data.Scale, data.Scale, 1);
    }
}